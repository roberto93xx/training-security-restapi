
package it.training.security.restapi.config;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path(value = "/test")
public class TestApi {

	@RolesAllowed(value = "admin")
	@GET
	@Path(value = "/helloadmin")
	public String helloAdmin() {
		return "hello by admin";
	}
	
	@RolesAllowed(value = { "admin", "guest"})
	@GET
	@Path(value = "/helloguest")
	public String helloGuest() {
		return "hello by guest";
	}
	
	@PermitAll
	@GET
	@Path(value = "/hello")
	public String helloeverybody() {
		return "helloooo";
	}
}
