package it.training.security.restapi;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath(value = "/api")
public class RestConfig extends Application { }
