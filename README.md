0. Scaricare JBOSS EAP 7.1
1. spostarsi dentro la cartella: jboss-eap-7.1\bin
2. Creare utenti e ruoli tramite lo script: add-user.bat su win o add-user.sh su unix-like (di default utente e ruoli afferiscono al realm
   di default di JBOSS EAP 7.1 che è il seguente: ApplicationRealm
3. Per verifica della creazione di utenti e ruoli:
	3_1. Spostarsi dentro la cartella jboss-eap-7.1\standalone\configuration 
	3_2. Aprire i 2 file: -> application-roles.properties -> application-roles.properties e verificare l'avvenuta creazione di utenze e ruoli

